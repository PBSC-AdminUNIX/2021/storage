Llave pública
- ['id_rsa.pub'](llave-publica/id_rsaAna.pub)

## Tarea día 2

Permisos de sistemas de archivos

![Permisos de sistemas de archivos](/img/dirs.jpg)
![Permisos](/img/perms.jpg)

Sudo. Políticas

![Políticas](/img/sudo.jpg)
