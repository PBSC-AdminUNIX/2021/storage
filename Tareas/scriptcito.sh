#!/bin/bash

message_error() {
    echo "Error en: $error"
}

user_exist() {
    user=$(echo $line | cut -f1 -d:)
    user_exis=$(id $user 2>/dev/null)
    if [ $? -eq 1 ]; then
        echo "El usuario no se ha encontrado"
	create_user
    else
        error="Ya existe el usuario $user "
        #ggnoteam
        mensaje_error
    fi
}

grupo_exist() {
    grupid=$(echo $line | cut -f4 -d:)
    grupi_ex=$(cat /etc/group | cut -f3 -d: | egrep "^$gid$")
    if [ $? -eq 0 ]; then
        user_exist
    else
        error="El grupo con el gip $grupid no es existe..."
        message_error
    fi
}

learch() {
    while IFS= read -r line; do
        grupo_exist
    done <$file
}

crear_user()
{
    home_u=$(echo $line | cut -f6 -d:)
    shell_u=$(echo $line | cut -f7 -d:)
    uid_u=$(echo $line | cut -f3 -d:)
    gecos_u=$(echo $line | cut -f5 -d:)
    pass_u=$(echo $line | cut -f2 -d:)
    pass_u=$(mkpasswd $pass)
    useradd -d $home_u -m -s $shell_u -u $uid_u -g $gid_u -c $gecos_u -p $pass_u $user_u
}


main() {
    if [ -n "$file" ]; then
        if [ -f "$file" ]; then
            learch
        else
            error="El archivo no existe"
            message_error
        fi
    else
        error="Por favor ingrese la ruta de un archivo"
        message_error
    fi
}

file=$1
main
