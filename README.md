# STORAGE

## Equipo:
-Gómez Hernández Erick Alexis

-Lara López Martha Leticia

-Méndez Gallegos Ligia Natalia

-Reynoso Gálvez Ana Laura

## Llaves públicas
-[`LeticiaLara_rsa.pub`](llave-publica/LeticiaLara_rsa.pub)
-[`id_rsaAna.pub`](llave-publica/id_rsaAna.pub)
-[`id_rsaNatalia.pub`](llave-publica/id_rsaNatalia.pub)

**************************************************
# Instalación y configuración de nfs V4
**************************************************
Para establecer una conexión entre los equipos vamos a compartir carpetas mediante NFS, para instalarlo debemos poner el comando:

`apt -qy install nfs-kernel-server` ---> debian

`yum install nfs-kernel-server` ----> centOS

Para revisar que la instalación sea correcta:
`dpkg nfs-kernel-server`

Creamos el direcctorio que después vamos a exportar:

`mkdir -vp /srv/nfs`

Y dentro de este directorio creamos un archivo para revisar lo que estará en NFS:

`touch /srv/nfs/inside-nfs`

Para exportar el directorio editamos el archivo /etc/exports:

![Alt text](img/nfs-tuto.png)

Comprobamos qué sistemas de archivos se exportan:

`showmount -e`

El equipo que importa los directorios debe hacer lo siguiente:

`mount -t nfs storage.planbecarios15.cf:/srv/ssl /srv/ssl` ---> Equipo web

`mount -t nfs storage.planbecarios15.cf:/srv/home /srv/home` ----> Equipo mail

`mount -t nfs storage.planbecarios15.cf:/srv/pki /srv/pki` ---> Equipo VPN

**************************************************
# Usuarios de NFS
**************************************************
## Configuración del cliente LDAP

Instalamos el paquete de soporte LDAP para PAM

`sudo apt-get install libnss-ldap libpam-ldap ldap-utils nscd -y`

En caso de que ya exista alguna configuración previa utilizar el siguiente comando para hacerlas desde cero.

`sudo apt-get purge libnss-ldap libpam-ldap ldap-utils nscd -y`

Nos muestra una ventana de Package Configuration donde debemos escribir el LDAP server URI:

`ldaps://directory.planbecarios15.cf:636/`

Configuramos el "LDAP server search base":

`dc=planbecarios15, dc=cf`

En las siguientes ventanas de configuración seleccionar las siguientes opciones:

- LDAP version to use: 3
- Make local root Database admin: Yes
- Does the LDAP database require login: No

A continuación especificar la cuenta de administrador para el servicio de LDAP:

`uid=storage,ou=users,dc=planbecarios15,dc=cf`

Después, ingresar el password del usuario anterior.

Para verificar las configuraciones hechas podemos ir al siguiente archivo:

`sudo cat /etc/nslcd.conf`

![nslcd](img/nslcd.conf.png)

## Ajustes de los archivos de configuración para LDAP

Abrir y editar el archivo

`sudo vi /etc/nsswitch.conf`

Verificar que existan las siguientes líneas, o en su caso agregar el sufijo ldap y las líneas faltantes:

`passwd: compat systemd ldap`
`group:  compat systemd ldap`
`shadow: files ldap`
`gshadow files`

Guardar los cambios.

Abrir y editar el archivo

`sudo vi /etc/pam.d/common-password`

Verificar que exista la siguiente línea o modificarla removiendo `use_authtok`:

`password [success=1 user_unknown=ignore default=die] pam_ldap.so try_first_pass`

Guardar los cambios.

Abrir y editar el archivo

`sudo nano /etc/pam.d/common-session`

Aqui agregar la siguiente línea al final del archivo:

`session optional pam_mkhomedir.so skel=/etc/skel umask=077`

Guadar los cambios.

Reiniciar el servidor con `sudo reboot`.

Una vez reiniciado el servidor comprobar que los usuarios LDAP sean visibles con el comando:

`getent passwd`

![getent](img/getent.png)


**************************************************
# BackupPC
**************************************************
 Primero es necesario instalar el servicio.

`apt install -y backuppc`

Seleccionamos la opción de

`local only`

Cambiamos la contraseña del usuario backuppc que se generó en la instalación.

`htpasswd /etc/backuppc/htpasswd backuppc`

Iniciamos el servicio

`systemctl start backuppc`

Verificamos el estado de BackupPC

`systemctl status backuppc`

El resultado es:


    backuppc.service - LSB: Launch backuppc server

     Loaded: loaded (/etc/init.d/backuppc; generated)

     Active: active (running) since Sun 2021-10-17 04:17:46 UTC; >

       Docs: man:systemd-sysv-generator(8)

      Tasks: 2 (limit: 1087)

     Memory: 29.4M

     CGroup: /system.slice/backuppc.service

             ├─66639 /usr/bin/perl /usr/share/backuppc/bin/Backup>

             └─66640 /usr/bin/perl /usr/share/backuppc/bin/Backup>


Cambiamos la configuración del archivo
`/etc/backuppc/config.pl`

Las líneas:

`
$Conf{CompressLevel} = 3` por
`$Conf{CompressLevel} = 7
`

Reiniciamos el servicio.

`sudo service backuppc restart
`

Modifiamos el archivo de apache2

`/etc/apache2/conf-available/backuppc.conf`

Cambiamos la línea:

`Require local` por
`Require all granted`

Reiniciamos el servicio.

`systemctl restart apache2`

Para conectarnos con los clientes debemos generar una llave para que los demás servicios nos reconozcan. Además, cada servicio debió crear un usuario 'backuppc'

`sudo -i`

`su - backuppc`

`ssh-keygen`

Y copiamos la llave a los demás servidores.

**Web:**

  `ssh-copy-id -i id_rsa.pub backuppc@web.planbecarios15.cf`

**DNS:**

  `ssh-copy-id -i id_rsa.pub backuppc@ns.planbecarios15.cf`

**Directory:**

  `ssh-copy-id -i id_rsa.pub backuppc@directory.planbecarios15.cf`

Ahora accedemos a
[13.58.149.146/backuppc](13.58.149.147/backuppc)

E iniciamos la configuración para generar los respaldos.
Agregamos a los clientes:

![Alt text](img/hosts.png)

Modificamos cada host especificando la ruta y el usuario que se usará para acceder al servidor a través de ssh.

**DNS:**

![Alt text](img/configDNS.png)


**Web:**
![Alt text](img/configWEB.png)

**Storage:**
![Alt text](img/configDirectory.png)


Depués de esto ya somos capaces de realizar el backup de cada servicio.
![Alt text](img/startBackup.png)

Los cuales se pueden ver reflejados en el apartado de Browse backups

![Alt text](img/seeBackups.png)

Al revisar la documentación con detenimiento, descubrimos que no es posible cambiar la ruta de donde se guardará el backup de cada cliente, por lo que realizamos una liga simbólica para los archivos de backup.
```
root@storage:/var/lib/backuppc/pc# ls -l
total 28
drwxr-x--- 4 backuppc backuppc 4096 Oct 17 20:46 ns.planbecarios15.cf
drwxr-x--- 2 backuppc backuppc 4096 Oct 18 02:00 storage.planbecarios15.cf
drwxr-x--- 3 backuppc backuppc 4096 Oct 18 00:17 web.planbecarios15.cf
```

Por lo que hacemos

```
$ ln -s ns.planbecarios15.cf /srv/backup/dns

$ ln -s web.planbecarios15.cf /srv/backup/mariadb

$ ln -s web.planbecarios15.cf /srv/backup/postgresql

$ ln -s storage.planbecarios15.cf /srv/backup/ldap
```

**************************************************
# To do:
**************************************************
Instalar y configurar el servidor NFS v4
- [x] Exportar el directorio /srv/ssl para guardar el certificado SSL que tramitó el equipo web
- [x] Exportar el directorio /srv/home para guardar los buzones de correo del equipo mail
- [x] Exportar un directorio bajo /srv/pki por cada equipo para guardar las llaves privadas y los certificados de VPN de cada servidor e integrantes de ese equipo
- [x] Exportar el directorio /srv/www
- [x] El equipo web lo montará en /var/www y guardará los htdocs de redmine y wordpress ahí

Usuarios de NFS

- [x] Debe configurarse la conexión a LDAP para que se guarden adecuadamente los buzones de correo
- [x] Configurar el propietario del directorio /srv/www como el usuario www-data
- [x] Verificar el usuario www-data tenga los mismos uid y gid en el equipo web y storage. Considerar el uso de las directivas all_squash, anonuid y anongid, ver man 5 exports

Instalar y configurar BackupPC desde paquetes con apt
- [x] Instalación y configuración de Backuppc
- [ ] Configurar el VirtualHost con un certificado SSL wildcard emitido por Let's Encrypt
Utilizar backuppc para copiar los respaldos de otros equipos
- [ ] Directorio LDAP (archivo ldif) generado por el equipo directory
- [x] Respaldos en formato SQL de MariaDB y PostgreSQL generado por el equipo web
- [x] Clonar y hacer pull del repositorio de la zona DNS
- [ ] Guardar los respaldos en /srv/backup y crear un directorio por cada servicio que se respalda:
  - [ ] ssl
  - [ ] ldap
  - [x] mariadb
  - [x] postgresql
  - [x] dns
