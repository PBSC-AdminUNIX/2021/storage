**************************************************
# To do:
**************************************************
Instalar y configurar el servidor NFS v4
- [x] Exportar el directorio /srv/ssl para guardar el certificado SSL que tramitó el equipo web
- [x] Exportar el directorio /srv/home para guardar los buzones de correo del equipo mail
- [x] Exportar un directorio bajo /srv/pki por cada equipo para guardar las llaves privadas y los certificados de VPN de cada servidor e integrantes de ese equipo
- [x] Exportar el directorio /srv/www
- [x] El equipo web lo montará en /var/www y guardará los htdocs de redmine y wordpress ahí

Usuarios de NFS

- [ ] Debe configurarse la conexión a LDAP para que se guarden adecuadamente los buzones de correo
- [x] Configurar el propietario del directorio /srv/www como el usuario www-data
- [x] Verificar el usuario www-data tenga los mismos uid y gid en el equipo web y storage. Considerar el uso de las directivas all_squash, anonuid y anongid, ver man 5 exports

Instalar y configurar BackupPC desde paquetes con apt
- [ ] Instalación y configuración de Backuppc
- [ ] Configurar el VirtualHost con un certificado SSL wildcard emitido por Let's Encrypt
Utilizar backuppc para copiar los respaldos de otros equipos
- [ ] Directorio LDAP (archivo ldif) generado por el equipo directory
- [ ] Respaldos en formato SQL de MariaDB y PostgreSQL generado por el equipo web
- [ ] Clonar y hacer pull del repositorio de la zona DNS
- [ ] Guardar los respaldos en /srv/backup y crear un directorio por cada servicio que se respalda:
  - [ ] ssl
  - [ ] ldap
  - [ ] mariadb
  - [ ] postgresql
  - [ ] dns
